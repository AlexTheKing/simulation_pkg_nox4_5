#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  7 16:37:29 2019

@author: alexander
"""


import pandas as pd
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import mygene
mg = mygene.MyGeneInfo()




tab = pd.read_table("Kinase_Substrate_Dataset1")

tab = tab.iloc[:].values

## Extracting protein from the Phosphoplus data base
## All isoform are changed to the gerneal protein
## A names containing "_" are removed - like "NP_579829"
new_interactions = []
c = 0
for i in tab:
    if i[3] =="human" and i[8] == "human":# and i[0] == 'PRKG1':
        if "_" in i[2] or "_" in i[6]:
            continue
        if "-" in i[2] and "-" in i[6]:
            new_interactions.append([i[2].split("-")[0],i[6].split("-")[0]])
        if "-" in i[2] and "-" not in i[6]:
            new_interactions.append([i[2].split("-")[0],i[6]])
        if "-" not in i[2] and "-" in i[6]:
            new_interactions.append([i[2],i[6].split("-")[0]])
        if "-" not in i[2] and "-" not in i[6]:
            new_interactions.append([i[2],i[6]])
        c += 1


print("number of kinase interactions:", c)





tab = pd.read_table("RegPhos_kinase_PPI_human.txt")
col_nms = list(tab.columns)
tab = tab.iloc[:].values

## Extracting protein from the Phosphoplus data base
## All isoform are changed to the gerneal protein
## A names containing "_" are removed - like "NP_579829"
regphos_interactions = []

c = 0
for i in tab:
    
    if "-" in i[4] or "-" in i[5]:
        print(i[4],i[5])
    
    regphos_interactions.append([i[4],i[5]])
    c += 1

print("number of regPhos kinase interactions:", c)



#g = gg.copy()
g = nx.Graph()

for i in new_interactions:
    g.add_edge(i[0],i[1])
    
for i in regphos_interactions:
    g.add_edge(i[0],i[1])
    

## Evi1 HNF-4alpha1 HNF-4alpha2 HOXA5 MIF-1 POU2F1 POU2F1a POU2F1b POU2F1c RFX1 JUN NFkB1 RELA STAT1 STAT3
## AP-1 ATF-2 c-Jun Nkx2-5 p53 E2F1 NFkB1 RELA SP3 STAT1 STAT3 STAT4


print("number of edges added:",g.number_of_edges())# - gg.number_of_edges())

print("number of nodes added:", g.number_of_nodes())# - gg.number_of_nodes())


print("isolating largest subnetwork:")
con = list(nx.connected_component_subgraphs(g))
for i in con:
    print(len(i))

g = con[0]
not_met = list(g)
not_met.sort()

print("the updated graph contains: ", len(g), len(not_met), "nodes")




nms = pd.read_table("IID_regphos_phosphosite_uniprot_to_symb.tab")
nms = nms.iloc[:].values
mapper = {}
rev_mapper = {}
for i in nms:
    mapper[i[0]] = str(i[1])
    rev_mapper[i[1]] = str(i[0])

#mapper["O43930"] = "5616"
#mapper["Q6A1A2"] = "653650"
#rev_mapper["5616"] = "O43930"
#rev_mapper["653650"] = "Q6A1A2"

gtt1 = g.copy()
print(len(gtt1))
gtt1 = nx.relabel_nodes(gtt1, mapper)
print(len(gtt1))


### There might stil be uniprot proteins in the graph
### to get them:
# len(set(list(g)).intersection(set(list(gtt1))))



# pkg1: 5592
# pkg2: 5593
#["PRKG1", "PRKG2"]
#["5592", "5593"]

pkg_targets = []
p = 0
for i in ["PRKG1", "PRKG2"]:
    pkg_targets.append(list(gtt1.neighbors(i)))
    print("length of PKG{} neighbors:".format(p+1),len(pkg_targets[p]))
    p += 1
    


## extracting data from the TRUST gene regulatory network
trust = pd.read_table('trrust_rawdata.human.tsv')
trust = trust.iloc[:].values
trust = trust.tolist()


"""
# commented out for the chapter

for i in range(len(trust)):
    if trust[i][0] == "STAT1" and trust[i][1] == "NOX4":
        trust[i][2] = "Activation"
    if trust[i][0] == "STAT3" and trust[i][1] == "NOX4":
        trust[i][2] = "Activation"


nx5 = ["HOXA5",
"MIF",
"POU2F1",
"RFX1"]

nx4 = ["ATF2",
"JUN",
"NKX2-5",
"TP53"]

for i in nx5:
    trust.append([i,"NOX5", "Unknown"])
for i in nx4:
    if i == "JUN":
        trust.append([i,"NOX4", "Activation"])
    else:
        trust.append([i,"NOX4", "Unknown"])
"""






G = nx.Graph()

for i in trust:    
    if i[2] == 'Activation':
        c = "green"
    if i[2] == 'Repression':
        c = "red"
    if i[2] == 'Unknown':
        c = "grey"
    G.add_edge(i[0], i[1],color = c)
    #G.add_edge(trust_mapper[i[0]],trust_mapper[i[1]],color = c)
print("Size of gene regulatory network:", len(G))
    
"""
GG = nx.Graph()
for i in trust:    
    if i[2] == 'Activation':
        c = "green"
        GG.add_edge(i[0], i[1],color = c)
    if i[2] == 'Repression':
        c = "red"
        GG.add_edge(i[0], i[1],color = c)
"""



def extract_neighbors(graph,neib):
    n= []
    for i in neib:
        n += list(graph.neighbors(i))
    return n
lst = ["NOX4", "NOX5"]
#lst = ["NOX4"]
#lst = ["NOX5"]
nei = extract_neighbors(G, lst)
nei = list(set(nei))



tr_G = nx.MultiDiGraph()
no_grey_G = nx.MultiDiGraph()
a1 = set()
a2 = set()
for i in trust:
    ## removing loops
    if i[0] == i[1]:
        continue
    if i[2] == 'Activation':
        c = "green"
        w = 1
    if i[2] == 'Repression':
        c = "red"
        w = -1
    if i[2] == 'Unknown':
        c = "grey"
        w = 0
    #tr_G.add_edge(trust_mapper[i[0]],trust_mapper[i[1]],color = c, weight = w)
    if (i[0], i[1], w) not in a1:
        if i[1] == "NOX4" or i[1] == "NOX5":
            if w == 0:
                a1.add((i[0], i[1], w) )    
                tr_G.add_edge(i[0], i[1],color = c, weight = w)
        if w != 0:
            a1.add((i[0], i[1], w) )    
            tr_G.add_edge(i[0], i[1],color = c, weight = w)
    if w != 0:
        #if (i[0], i[1], w) not in a2:
        no_grey_G.add_edge(i[0], i[1],color = c, weight = w)
        a2.add((i[0], i[1], w) ) 


#tr_G.remove_edge("E2F1", "TP53") # commented out for the chapter
#tr_G.remove_edge("E2F1", "TP53") # commented out for the chapter
tr_g = tr_G.subgraph(nei + lst)
nx.drawing.nx_pydot.write_dot(tr_g, 'my_graph.dot')
edges_between_TF_and_NOX = list(tr_g.edges())
## run in terminal: dot my_graph.dot -T png > output.png

nei_no_grey = nei.copy()
nei_no_grey.remove("STAT4")
tr_g = no_grey_G.subgraph(nei_no_grey + lst)
nx.drawing.nx_pydot.write_dot(tr_g, 'my_graph1.dot')
edges_between_TF_and_NOX = list(tr_g.edges())



## Finding the PKG1/2 targets that are in the trust gene reg. network.
in_tr = []
for i in pkg_targets:
    tmp = []
    for ii in i:
        if ii in no_grey_G:#tr_G:
            tmp.append(ii)
    in_tr.append(tmp)

for i in range(len(pkg_targets)):
    print("PKG{} - from".format(i+1),len(pkg_targets[i]), "to", len(in_tr[i]))




## I will use all proteins in the paths to create a subgraph. Maybe not..
## I will remove all loops and remove Unknown regulation.
## From here, I analyze the regulation in timesteps...
## add af factor of regulation
## assuming PKG phosphorylation activates
## does not "add" regualtions if one node regulates another multiple times
## should normalize based on real protein abundance

## finding all paths from PKG1/2 targets to NOX4/5 TFs
#test_G = nx.MultiDiGraph()#tr_G.copy()
T = in_tr[0] #+ in_tr[1]
no_paths = []
pths = []
for i in T:
    for ii in nei:
        try:
            pths += list(nx.all_shortest_paths(no_grey_G, i, ii)) 
        except:
            no_paths.append([i,ii])


T.remove("RHOA")
pkgT = T


## Cancel out RHOA, as it is inhibited by PKG1





all_genes = list(set(list(no_grey_G.nodes())))
all_genes.sort()


## extracting expression values to create an estiame of prot abundance
cntr_dat = pd.read_csv("annotated_ctrl_exprs_stacked.tsv", sep="\t")
rep_size = int(cntr_dat.shape[0]/5)
cntr_dat = cntr_dat.iloc[:].values
entrez = cntr_dat[:rep_size,0]
entrez = list(entrez)
entrez = [int(i) for i in entrez]
cntr_dat = cntr_dat[:,1:]
cntr_dat= cntr_dat.reshape((rep_size,5,10))
cntr_dat = cntr_dat/np.max(cntr_dat)

BN = np.average(cntr_dat)
BS = np.std(cntr_dat)

"""
entids = {}
DQ = 1
for i in all_genes:
    for ii in mg.query(i)["hits"]:
        if ii["taxid"] == 9606:
            if "entrezgene" in ii:
                DQ = 0
                entids[i] = ii["entrezgene"]
                continue
    if DQ == 1:
        entids[i] = "RAN"
    print(i, sep=" ", end= " ")

"""
### loading a conversion list of gene symbols to entrez IDs
import pickle
with open("entids.pkl", "rb") as FFFF:
    entids = pickle.load(FFFF)

np.random.seed(111)
distribution = {}
for k,v in entids.items():
    if int(v) in entrez:
        distribution[k] = cntr_dat[entrez.index(int(v))]
    else:# v == "RAN":
        DDD = np.random.normal(BN, BS,size=(5,10))
        DDD[DDD < 0] = BS
        distribution[k] = DDD

for i in all_genes:
    if i not in distribution:
        print(i)
        DDD = np.random.normal(BN, BS,size=(5,10))
        DDD[DDD < 0] = BS
        distribution[i] = DDD


"""
## Initializing the protein abundance at random
np.random.seed(111)
distribution = {}
for k,v in entids.items():
    if int(v) in entrez:
        distribution[k] = np.random.uniform(low=0.0001, high=1.0, size=(5,10))
    else:# v == "RAN":
        DDD = np.random.uniform(low=0.0001, high=1.0, size=(5,10))
        #DDD[DDD < 0] = BS
        distribution[k] = DDD

for i in all_genes:
    if i not in distribution:
        print(i)
        DDD = np.random.uniform(low=0.0001, high=1.0, size=(5,10))
        #DDD[DDD < 0] = BS
        distribution[i] = DDD
"""



nei_lst = nei+lst

GG = set(no_grey_G.edges())
GG_g = nx.Graph(no_grey_G)

def calc_regulation(all_genes, distribution, a, b, rate = 0, factor = 0.3):

    """

    """


    def scaled_tanh(x,y):
        return ((np.exp(x+y)-np.exp(-x+y)))/(np.exp(x)+np.exp(-x))


    def calc_start_prot_abundance(gnnms, dist, a, b):

        abundance = []
        for i in gnnms:
            abundance.append(dist[i][a][b])
        return np.array(abundance)


    start = 2
    p1 = 0
    p2 = 1
    #factor = 0.3


    abundance = calc_start_prot_abundance(all_genes,distribution,a,b)
    start_ab = abundance.copy()
    #print(len(max(pths,key=len)) + len(max(pths,key=len)) - len(min(pths,key=len)))
    tmp_targ = pkgT.copy()
    print(tmp_targ)
    while start < len(max(pths,key=len)):# + len(max(pths,key=len)) - len(min(pths,key=len)):
        step = []
        rem = set()
        new_targ = set()
        for i in tmp_targ:
            tmp_nb = list(GG_g.neighbors(i))   
            tmp_val = 0           
            for nb in tmp_nb:
                if (i, nb) in GG:
                    new_targ.add(nb)
                    for v in range(len(no_grey_G[i][nb])):
                        tmp_val += no_grey_G[i][nb][v]["weight"]
                    if tmp_val > 0:
                        step.append([all_genes.index(nb), 1 + (scaled_tanh(abundance[all_genes.index(i)]*factor, rate)/(1))])
                    if tmp_val < 0:
                        step.append([all_genes.index(nb), 1 / (1 + (scaled_tanh(abundance[all_genes.index(i)]*factor, rate)/(1)))])

        for up in step:
            abundance[up[0]] *= up[1]

        p1 += 1
        p2 += 1
        start += 1
        tmp_targ = list(new_targ)


    all_max_vals = np.max(abundance)
    
    D = []
    tmp = []
    true_abundance = []
    for i in range(len(abundance)):
        if all_genes[i] in nei_lst:
            print(all_genes[i], np.log2(abundance[i]/start_ab[i] ))
            D.append([all_genes[i], np.log2(abundance[i]/start_ab[i])])
            tmp.append(np.log2(abundance[i]/start_ab[i]))
            true_abundance.append([all_genes[i], abundance[i],start_ab[i]])

    av = np.average(tmp)
    print(av)
    print("\n")

    return D, av, true_abundance, all_max_vals








a = cntr_dat.shape[1]
b = cntr_dat.shape[2]
allmaxvals = []
TT = []
TFS = []
TR_AB = []
scale = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.]
for F in scale:
    tfs = {}
    avs = []
    dd = []
    trab = []
    amv = []
    n = 0
    t = []
    for r in [0]:
        tt = {}
        #trab_tmp = []
        for H in nei_lst:
            tt[H] = 0
            
        for i in range(a):
            for ii in range(b):
                tf, av, tr_ab, almvals = calc_regulation(all_genes, distribution, i, ii, r, factor = F)
                n += 1
                amv.append(almvals)
                trab.append(tr_ab)
                for Q in tf:
                    tt[Q[0]] += Q[1]
                    
                    if Q[0] not in tfs: ## I can define it like in line 472..
                        tfs[Q[0]] = Q[1]
                        
                    else:
                        tfs[Q[0]] += Q[1]
                avs.append(av)
                #dd.append(tf)
        t.append(tt)
        TR_AB.append(trab)
        allmaxvals.append(amv)
    
    for i in t:
        for k,v in i.items():
            i[k] = v/(a*b)
    TT.append(t)
    
    
    for k,v in tfs.items():
        tfs[k] = v/n
    TFS.append(tfs)
    

BB = {}
for i in TFS:
    for k,v in i.items():
        BB[k] = 0

for i in TFS:
    for k,v in i.items():
        BB[k] += v

for k,v in BB.items():
    BB[k] = v/len(scale)







sim_ab = []
real_ab = []

control = {}
for i in TR_AB[0][0]:
    control[i[0]] = 0

for i in TR_AB:
    simab = control.copy()
    realab = control.copy()
    for ii in i:
        for iii in ii:
            simab[iii[0]] += iii[1]
            realab[iii[0]] += iii[2]
    sim_ab.append(simab)
    real_ab.append(realab)
    
for i in range(len(sim_ab)):
    for k,v in sim_ab[i].items():
        sim_ab[i][k] = v/50
        
for i in range(len(real_ab)):
    for k,v in real_ab[i].items():
        real_ab[i][k] = v/50
        

for i in range(len(sim_ab)):
    print("\n")
    for k,v in sim_ab[i].items():
        print(k, v, real_ab[i][k], np.log2(v/real_ab[i][k]))
    #print("\n")


av_sim = []
av_real = []
for i in TR_AB:
    tmp_sim = control.copy()
    tmp_real = control.copy()
    for ii in i:
        for iii in ii:
            tmp_real[iii[0]] += iii[2]
            tmp_sim[iii[0]] += iii[1]
    av_sim.append(tmp_sim)
    av_real.append(tmp_real)
    
avav_sim_real = []
for i in range(len(av_sim)):
    tmp = []
    print("\n\n")
    for k,v in control.items():
        print(k, av_sim[i][k], av_real[i][k], np.log2(av_sim[i][k]/av_real[i][k]))
        tmp.append([k, av_sim[i][k], av_real[i][k], np.log2(av_sim[i][k]/av_real[i][k])])
    avav_sim_real.append(tmp)
    
avavav_sim = control.copy()
avavav_real = control.copy()
for i in avav_sim_real:
    for ii in i:
        avavav_sim[ii[0]] += ii[1]
        avavav_real[ii[0]] += ii[2]


# final log2 difference
for k,v in control.items():
    print(k,np.log2(avavav_sim[k]/avavav_real[k]))


nx_5 = ["STAT1", "STAT3", "JUN", "NFKB1","RELA"]
nx_4 = ["NFKB1","RELA", "E2F1", "SP3"]
"""
all_real_n4 = []
all_real_n5 = []

for i in s:
    tmp_n4 = []
    tmp_n5 = []
    for ii in [0]:
        for iii in TR_AB[0]:
            nox4 = iii[3][2]
            nox5 = iii[4][2]
            for iiii in iii:
                if iiii[0] in nx_4:
                    nox4 *= (1+scaled_tanh(iiii[2]*i, 0))
                    #tmp_n4.append(iii[3][2]*(1+scaled_tanh(iiii[2]*i, 0)))
                if iiii[0] in nx_5:
                    nox5 *= (1+scaled_tanh(iiii[2]*i, 0))
                    #tmp_n5.append(iii[4][2]*(1+scaled_tanh(iiii[2]*i, 0)))
            tmp_n4.append(nox4)
            tmp_n5.append(nox5)
    all_real_n4.append(tmp_n4)
    all_real_n5.append(tmp_n5)





allrn4 = np.array(all_real_n4)
allrn5 = np.array(all_real_n5)


simulsn4 = []
simulsn5 = []
for i in TR_AB:
    t4 = []
    t5 = []
    for ii in i:
        t4.append(ii[3][1])
        t5.append(ii[4][1])
    simulsn4.append(t4)
    simulsn5.append(t5)
    
simulsn4 = np.array(simulsn4)
simulsn5 = np.array(simulsn5)


plt.plot(np.average(simulsn4,axis=0))
plt.plot(np.average(allrn4,axis=0))

"""


def scaled_tanh(x,y):
    return ((np.exp(x+y)-np.exp(-x+y)))/(np.exp(x)+np.exp(-x))

s = scale
## Figures ####################################################
    
"""   
TX = []
for i in TT:
    TX += i

lab = []
x = np.zeros((len(TX[0]), len(TX)))
I = -1
for k,v in TX[0].items():
    lab.append(k)
    I += 1
    for i in range(len(TX)):
        x[I][i] += TX[i][k]
"""


x = []
lab = [i[0] for i in avav_sim_real[0]]
for i in range(len(s)):
    tmp = []
    for ii in avav_sim_real:
        tmp.append(ii[i][3])
    x.append(tmp)


col = ["red", "g", "blue", "yellow", "orange", "purple","k","brown", "grey", "pink", "teal", "silver", "darkred", "darkgoldenrod", "darkgreen"]


plt.figure(figsize=(17,10))
ax = plt.subplot(111)
for i in range(len(x)):
    if np.sum(x[i]) != 0:
        if lab[i] != "NOX4" and lab[i] != "NOX5" and lab[i] != "STAT4":
            ax.plot(x[i], label=lab[i], color = col[i])

plt.legend(fontsize=20)
plt.ylabel("Log2 difference", fontsize=20)
plt.xlabel("Scaling sizes", fontsize=20)
my_xticks = ['0.1', '0.2', '0.3', '0.4','0.5','0.6','0.7','0.8','0.9','1.0']
plt.xticks(list(range(len(my_xticks))), my_xticks)
chartBox = ax.get_position()
ax.set_position([chartBox.x0, chartBox.y0, chartBox.width*0.6, chartBox.height])
ax.legend(loc='upper center', bbox_to_anchor=(1.1, 1.), shadow=True, ncol=1, fontsize=15)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.savefig("regulation_vs_scaling.svg")
    
    
    
#################################

#find abundance per scaling factor
x = []
lab = [i[0] for i in avav_sim_real[0]]
for i in range(len(s)):
    tmp = []
    for ii in TR_AB:
        tmp2 = []
        for iii in ii:
            tmp2.append(np.log2(iii[i][1]/iii[i][2]))
        tmp.append(tmp2)
    x.append(tmp)


plt.figure(figsize=(17,10))
ax = plt.subplot(111)
for i in range(len(x)):
    if np.sum(x[i]) != 0:
        if lab[i] != "NOX4" and lab[i] != "NOX5" and lab[i] != "STAT4":
            ax.plot(np.average(x[i],axis=0), label=lab[i], color = col[i])


plt.legend(fontsize=20)
plt.ylabel("Log2 difference", fontsize=20)
plt.xlabel("Number of simulations", fontsize=20)
#my_xticks = ['0.1', '0.2', '0.3', '0.4','0.5','0.6','0.7','0.8','0.9','1.0']
#plt.xticks(list(range(len(my_xticks))), my_xticks)
chartBox = ax.get_position()
ax.set_position([chartBox.x0, chartBox.y0, chartBox.width*0.6, chartBox.height])
ax.legend(loc='upper center', bbox_to_anchor=(1.1, 1.), shadow=True, ncol=1, fontsize=15)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
hlin = [0, 9, 19, 29, 39, 49]
for i in hlin:
    plt.vlines(ymax=8, ymin=-5, x=i)
plt.savefig("regulation_50.svg")

##########################################


xx = []
names = []
for i in range(len(x)):
    if lab[i] != "NOX4" and lab[i] != "NOX5" and lab[i] != "STAT4":
        names.append(lab[i])
        xx.append(np.average(x[i],axis=0))
    
xx = np.array(xx)

corr = np.corrcoef(xx)

print(names)
for i in range(len(corr)):
    print(corr[i][i+1:])


seqment_corr = []
for ii in range(5):
    seqment_corr.append(np.corrcoef(xx[:,ii*10:ii*10+10]))


with open("coors.csv", "w") as f:
    f.write(",")
    for j,i in enumerate(names):
        if j == len(names)-1:
            f.write(i)   
        else: 
            f.write(i+",")
        
    for i in seqment_corr:
        f.write("\n")
        for jjj,iii in enumerate(i):
            print(iii)
            f.write(names[jjj]+",")
            for jj,ii in enumerate(iii):
                print(ii, sep=" ")
                if jj == len(iii)-1:
                    f.write(str(np.round(ii,2)))   
                else: 
                    f.write(str(np.round(ii,2))+",")
            f.write("\n")

##########################################
            
av_of_corr = 0
for i in seqment_corr:
    av_of_corr += i

av_of_corr /= 5


cor = [av_of_corr]
with open("av_of_rep_corr.csv", "w") as f:
    f.write(",")
    for j,i in enumerate(names):
        if j == len(names)-1:
            f.write(i)   
        else: 
            f.write(i+",")

    for i in cor:
        f.write("\n")
        for jjj,iii in enumerate(i):
            print(iii)
            f.write(names[jjj]+",")
            for jj,ii in enumerate(iii):
                print(ii, sep=" ")
                if jj == len(iii)-1:
                    f.write(str(np.round(ii,2)))   
                else: 
                    f.write(str(np.round(ii,2))+",")
            f.write("\n")



##########################################

## getting the data as according to GSE real data
xz = []
lab = [i[0] for i in avav_sim_real[0]]
for i in range(len(s)):
    tmp = []
    for ii in TR_AB:
        tmp2 = []
        for iii in ii:
            tmp2.append(iii[i][2])
        tmp.append(tmp2)
    xz.append(tmp)


plt.figure(figsize=(17,10))
ax = plt.subplot(111)
for i in range(len(xz)):
    if np.sum(xz[i]) != 0:
        if lab[i] != "NOX4" and lab[i] != "NOX5" and lab[i] != "STAT4":
            ax.plot(np.average(xz[i],axis=0), label=lab[i], color = col[i])


## Gettign the average simulated data "as is"
xzz = []
lab = [i[0] for i in avav_sim_real[0]]
for i in range(len(s)):
    tmp = []
    for ii in TR_AB:
        tmp2 = []
        for iii in ii:
            tmp2.append(iii[i][1])
        tmp.append(tmp2)
    xzz.append(tmp)


plt.figure(figsize=(17,10))
ax = plt.subplot(111)
for i in range(len(xz)):
    if np.sum(xzz[i]) != 0:
        if lab[i] != "NOX4" and lab[i] != "NOX5" and lab[i] != "STAT4":
            ax.plot(np.log2(np.average(xzz[i],axis=0)), label=lab[i], color = col[i])
            

##########################################
    
    
r = [0,0]#,rates[5],rates[2]]
pr = [0,0,0]#]-0.51,-1.20]
#s = [1,0.6]#,0.1, 0.1]
s = scale

x = np.arange(0,30,0.1)
plt.figure(figsize=(15,12))
for i in range(len(scale)):
    x1 = 1/(1+scaled_tanh(x*s[i], 0))
    plt.plot(x,1+scaled_tanh(x*s[i],0), c=col[i], label="z = {}".format(s[i]))
    plt.plot(x,x1, c=col[i])
    #patch = mpatches.Patch(color=col[i], label="Up {}, Down {}".format(1+scaled_tanh(1000*0.3, rates[i]), 1/(1+scaled_tanh(1000*0.3, rates[i]))))
    plt.legend(fontsize=20)


plt.ylim(0.3,2.2)
plt.xlim(-0.7,30)
plt.hlines(y = 1, xmin = -1,xmax = 30, lw=3)
plt.title("Regulation magnitudes",fontsize=24)
plt.xlabel("Protein abundance",fontsize=20)
plt.ylabel("Regulation size",fontsize=20)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.savefig("scaled_regulation.svg") 





lab = []
x = np.zeros((len(t[0]), len(t)))
I = -1
for k,v in t[0].items():
    lab.append(k)
    I += 1
    for i in range(len(t)):
        x[I][i] += t[i][k]

col = ["red", "g", "blue", "yellow", "orange", "purple","k","brown", "grey", "pink", "teal", "silver", "darkred", "darkgoldenrod", "darkgreen"]


plt.figure(figsize=(13,10))
ax = plt.subplot(111)
for i in range(len(x)):
    ax.plot(x[i], label=lab[i], color = col[i])

plt.legend(fontsize=18)
plt.ylabel("log2 difference", fontsize=18)
plt.xlabel("max regulation size", fontsize=18)
my_xticks = ['1.1', '1.2', '1.3', '1.4','1.5','1.6','1.7','1.8','1.9','2.0']
plt.xticks(list(range(len(my_xticks))), my_xticks)
chartBox = ax.get_position()
ax.set_position([chartBox.x0, chartBox.y0, chartBox.width*0.6, chartBox.height])
ax.legend(loc='upper center', bbox_to_anchor=(1.1, 1.), shadow=True, ncol=1)
plt.savefig("max_reg_logdiff1.svg")










pkgs = ["PRKG1", "PRKG2"]
for i in range(len(pkgs)):
    for ii in in_tr[i]:
        tr_G.add_edge(pkgs[i], ii, color = "purple")


#TT = pths[:3]+ pths[18:21]+ pths[8:10]#+ [["PRKG1"]] + [lst]

TT = []
ways = []
for i,j in enumerate(pths):
    if j[-1] == "NFKB1":
        print(i,j)
        ways.append(i)
        
for i in ways:
    if i != 51 and i != 52:
        print(i)
        TT += pths[i]

#TTT = []
#for i in TT:
#    TTT += i
    
TT += ["NOX4", "NOX5", "PRKG1"]


tr_g = tr_G.subgraph(TT)
nx.drawing.nx_pydot.write_dot(tr_g, 'my_graph_sample1.dot')






{'E2F1': -3.6450845644975454,
 'JUN': 4.4235823768514155,
 'NFKB1': -2.1514317387808304,
 'RELA': -1.7412012955939324,
 'SP3': 0.21561031162843416,
 'STAT1': 0.7621575634479504,
 'STAT3': -2.0139629509862464,
 'STAT4': 0.7297471091951497}






nx_5 = ["STAT1", "STAT3", "JUN", "NFKB1","RELA"]

nx_4 = ["NFKB1","RELA", "E2F1", "SP3"]

m4 = 0
for i in nx_4:
    m4 += BB[i]
print(m4, m4/len(nx_4))

m5 = 0
for i in nx_5:
    m5 += BB[i]
print(m5, m5/len(nx_5))


m = 0
test = nx_4 + nx_5
test = list(set(test))
for i in test:
    m += BB[i]
    
print(m, m/len(test))


#-7.322107287243874 -1.8305268218109685
#-0.7208560450616435 -0.1441712090123287
# -4.150330297930755 -0.5929043282758222
